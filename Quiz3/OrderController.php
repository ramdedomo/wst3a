<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function customer($id, $name, $address)
    {
        //
        return 
        "
        Ram Wendel Dedomo<br>
        BSIT - 3A<br><br>

        Customer<br><br>

        
        CustomerID:<input type='text' value='".$id."'><br>
        CustomerName:<input type='text' value='".$name."'><br>
        Address:<input type='text' value='".$address."'><br>
        ";

    }

    public function item($no, $name, $price)
    {
        //
        return 
        "
        Ram Wendel Dedomo<br>
        BSIT - 3A<br><br>

        Item<br><br>

        ItemNo:<input type='text' value='".$no."'><br>
        ItemName:<input type='text' value='".$name."'><br>
        Price:<input type='text' value='".$price."'><br>
        ";

    }

    public function order($id, $name, $order, $date)
    {
        //
        return 
        "
        Ram Wendel Dedomo<br>
        BSIT - 3A<br><br>

        Order<br><br>

        CustomerID:<input type='text' value='".$id."'><br>
        ItemName:<input type='text' value='".$name."'><br>
        OrderNo:<input type='text' value='".$order."'><br>
        Date:<input type='text' value='".$date."'><br>
        ";
    }

    public function orderdetails($trans, $order, $itemid, $name, $price, $qty)
    {
        //{trans}/{order}/{itemid}/{name}/{price}/{qty}
        return 
        "
        Ram Wendel Dedomo<br>
        BSIT - 3A<br><br>

        Order Details<br><br>

        TransNo:<input type='text' value='".$trans."'><br>
        OrderNo:<input type='text' value='".$order."'><br>
        ItemID:<input type='text' value='".$itemid."'><br>
        ItemName:<input type='text' value='".$name."'><br>
        Price:<input type='text' value='".$price."'><br>
        Qty:<input type='text' value='".$qty."'><br>
        ";
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
