<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AdminAppointments;
use App\Models\Administrator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Appointments;
use Illuminate\Validation\Rule;


class CRUDdb extends Controller
{

    function confirmupdateservices(Request $request, $id){

        $request->validate([
            'productname'=>'required',
            'price'=>'required',
            'qty'=>'required',
            'productdesc'=>'required'
        ]);

        $update = DB::table('items')
        ->where('admin_appointment_id', $id)
        ->update([

        'admin_appointment_name' => $request->productname,
        'admin_appointment_desc' => $request->productdesc,
        'stocks' => $request->qty,
        'price' => $request->price

        ]);

       
        if($update){
            return back()->with('success','Updated');
        }else{
            return back()->with('fail','Something went wrong, Try again later');
        }


    }

    function editService($id){

        $appData = DB::table('items')
        ->where('admin_appointment_id', $id)
        ->select("*")
        ->get();

        return view('editservices', ['edit'=>$appData]);
    }

    function addservices(){
        return view('addservices');
    }

    function confirmaddservices(Request $request){

        $id =   Administrator::where('admin_username','=', session('sessionUser'))->get('admin_id');

        $request->validate([
            'productname'=>'required',
            'price'=>'required',
            'qty'=>'required',
            'productdesc'=>'required'
        ]);

        $date = date_default_timezone_set('Asia/Manila');
        $insert_date = date('Y-m-d H:i:s');

        $insert = DB::table('items')->insert([
            'admin_appointment_name' => $request->productname,
            'admin_appointment_desc' => $request->productdesc,
            'stocks' => $request->qty,
            'admin_id' => $id[0]['admin_id'],
            'date_created' => $insert_date,
            'price' => $request->price
        ]);
            
            if($insert){
                return back()
                ->with('success','Product Added!');
            }else{
                 return back()->with('fail','Something went wrong, Try again later');
            }
        
    }

    function occupied($id){
        $update = DB::table('transanctions')
              ->where('appointment_id', $id)
              ->update(['status' => 2]);

              return back();

    }

    function cancel($id){
        $update = DB::table('transanctions')
              ->where('appointment_id', $id)
              ->update(['status' => 3]);

              return back();

    }

    function approved($id){
        $update = DB::table('transanctions')
            ->where('appointment_id', $id)
            ->update(['status' => 1]);

            return back();
    }



    function search(Request $request){

            $id =   Administrator::where('admin_username','=', session('sessionUser'))->get('admin_id');
            $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

            $yourappData = DB::table('transanctions')
            ->join('user', 'transanctions.user_id', '=', 'user.user_id')
            ->join('items',  'items.admin_appointment_id', '=', 'transanctions.admin_appointment_id')
            ->select("*")
            ->where('transanctions.admin_id','=', $id[0]['admin_id'])
            ->where('transanctions.appointment_date', 'LIKE', "%$request->search%")
            ->orWhere('transanctions.appointment_time', 'LIKE', "%$request->search%")
            ->get();

            return view('admin.admin_appointments', ['status'=>'1'], ['your_appointmentData'=>$yourappData], ['sessionUserInfo'=>$data]);

    }

    

    function admin(){
        $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

        $appData = DB::table('items')
        ->select("*")
        ->get();

        return view('admin.indexadmin', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);
    }

    function adminappointments(){

        $id =   Administrator::where('admin_username','=', session('sessionUser'))->get('admin_id');
        $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

        $yourappData = DB::table('transanctions')
        ->join('user', 'transanctions.user_id', '=', 'user.user_id')
        ->join('items',  'items.admin_appointment_id', '=', 'transanctions.admin_appointment_id')
        ->select("*")
        ->where('transanctions.admin_id','=', $id[0]['admin_id'])
        ->get();

        $user = DB::table('user')
        ->select("*")
        ->get();
        
        return view('admin.admin_appointments', ['status'=>'0'], ['your_appointmentData'=>$yourappData, 'user'=>$user], ['sessionUserInfo'=>$data]);
    }

    function adminprofile(){
        $userdata = Administrator::where('admin_username','=', session('sessionUser'))->first();
        return view('admin.admin_profile', ['userdata'=>$userdata]);
    }
    
    function logout(){
        if(session()->has('sessionUser')){
            session()->pull('sessionType');
            session()->pull('sessionUser');
            return redirect('login');
        }
    }

    function login(){
        // $session = ['sessionValue'=>session('sessionUser')];
        return view('login');
    }

    function register(){
        return view('register');
    }


    function index(){
        $session = session('sessionUser');
        $data = User::where('username','=', session('sessionUser'))->first();

        $appData = DB::table('items')
        ->select("*")
        ->get();

        return view('index', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);
    }

    function yourappointments(){
        
        $id = User::where('username','=', session('sessionUser'))->get('user_id');
        $data = User::where('username','=', session('sessionUser'))->first();

        $yourappData = DB::table('transanctions')
        ->join('items', 'transanctions.admin_appointment_id', '=', 'items.admin_appointment_id')
        ->select("*")
        ->where('transanctions.user_id','=', $id[0]['user_id'])
        ->get();

        return view('yourappointments', ['your_appointmentData'=>$yourappData], ['sessionUserInfo'=>$data]);
    }

    function profile(){
        $userdata = User::where('username','=', session('sessionUser'))->first();
        return view('profile', ['userdata'=>$userdata]);
    }


    function save(Request $request){

        if($request->password != $request->confirmpassword){
            return back()->with('fail','Password Confirmation does not Match!');
        }else{

            $request->validate([
                'firstname'=>'required',
                'lastname'=>'required',
                'phone'=>'required|numeric',
                'username'=>'required|unique:user',
                'email'=>'required|email',
                'password'=>'required'
            ]);

            //  Insert data into database
            $user = new user;
            $user ->firstname = $request->firstname;
            $user ->lastname = $request->lastname;
            $user ->phone = $request->phone;
            $user ->username = $request->username;
            $user ->email = $request->email;
            $user ->password = $request->password;
            $insert = $user ->save();

            if($insert){
                return back()->with('success','Registered');
            }else{
                return back()->with('fail','Something went wrong, Try again later');
            }

        }
        
    

    }

    function addappoint($id, Request $request){

            $appData = DB::table('items')
            ->where('admin_appointment_id', $id)
            ->select("*")
            ->get();

            $stocks = $appData[0]->stocks;


            $appointmentSelected = DB::table('items')
            ->where('admin_appointment_id','=', $id)
            ->select("*")
            ->get();

            $user_id = User::where('username','=', session('sessionUser'))->get('user_id');

            if($request->quantity > $appData[0]->stocks || $request->quantity < 0){
                return back()->with('fail','Available Stocks: ' . $appData[0]->stocks);
            }


            $request->validate([
                'quantity'=>'required'
            ]);

            $date = date_default_timezone_set('Asia/Manila');
            $insert_date = date('Y-m-d H:i:s');

            $insert = DB::table('transanctions')->insert([
                    'user_id' => $user_id[0]->user_id,
                    'admin_id' => $appointmentSelected[0]->admin_id,
                    'admin_appointment_id' => $id,
                    'qty' => $request->quantity,
                    'date_puchased' => $insert_date
            ]);

            
            $update = DB::table('items')
            ->where('admin_appointment_id', $id)
            ->update(['stocks' => $stocks - $request->quantity]);

                if($insert){
                    return back()->with('success','Status: Approved');
                }else{
                     return back()->with('fail','Something went wrong, Try again later');
                }

    }

    function home() {
        if(session('sessionType') == "user"){

            $session = session('sessionUser');
            $data = User::where('username','=', session('sessionUser'))->first();
    
            $appData = DB::table('items')
            ->select("*")
            ->get();
    
            return view('index', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);

        }
        elseif(session('sessionType') == "admin"){

            $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

            $appData = DB::table('items')
            ->select("*")
            ->get();
    
            return view('admin.indexadmin', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);

        }
        else{
            return view('login');
        }
    }


    function delete($id){
            $delete = DB::table('transanctions')->where('appointment_id', '=', $id)->delete();

            if($delete){
                return back()->with('success','Appoinment Deleted!');
             }else{
                 return back()->with('fail','Something went wrong, Try again later');
             }
    }

    function deleteService($id){
        $delete = DB::table('items')->where('admin_appointment_id', '=', $id)->delete();

        if($delete){
            return back()->with('success','Service Deleted!');
         }else{
             return back()->with('fail','Something went wrong, Try again later');
         }
}


    function appoint($id){

        $data = User::where('username','=', session('sessionUser'))->first();

        $appointmentSelected = DB::table('items')
        ->select("*")
        ->where('items.admin_appointment_id','=', $id)
        ->first();

        return view('appoint', ['appointment_Selected'=>$appointmentSelected], ['sessionUserInfo'=>$data]);
    }


    function check(Request $request){
        //Validate requests
        $request->validate([
             'username'=>'required',
             'password'=>'required'
        ]);
        
        $userInfo = User::where('username','=', $request->username)->first();
        //$userID = User::where('username','=', $request->username)->first(['user_id']);
        $adminInfo = Administrator::where('admin_username','=', $request->username)->first();

        if(!$userInfo){
            if(!$adminInfo){
            return back()->with('fail','Incorrect username or password');
            }else{
                if($request->password == $adminInfo->admin_password){
                    $request->session()->put('sessionType', 'admin');
                    $request->session()->put('sessionUser', $adminInfo->admin_username);
                    return redirect('admin');
                }else{
                    return back()->with('fail','Incorrect username or password');
                }
            }
        }else{

            //check password
            if($request->password == $userInfo->password){
                $request->session()->put('sessionType', 'user');
                $request->session()->put('sessionUser', $userInfo->username);
                return redirect('index');
            }else{
                return back()->with('fail','Incorrect username or password');
            }

        }

        
    }



}
