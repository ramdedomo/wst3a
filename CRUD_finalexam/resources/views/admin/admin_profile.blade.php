<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</head>
<body class="bg-dark">



  <div class="container">
    <span class="badge bg-primary mt-5">ADMINISTRATOR</span>

      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/admin">Products</a>
          </li>
          <li class="nav-item bg-primary rounded m-2  bg-opacity-10">
            <a class="nav-link text-light" href="/adminappointments">Orders & Users</a>
          </li>
          <li class="nav-item bg-primary rounded m-2">
            <a class="nav-link text-light" href="/adminprofile">Profile</a>
          </li>

        </ul>

          <hr class="mt-3 mx-2" style="color: white">
          
      </div>

      <div class="shadow p-3 bg-light text-dark col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2 position-absolute top-50 start-50 translate-middle rounded">
        <div class="fw-bold">Profile Details</div>
        <p style="font-size: 10px">CRUD</p>
        <hr>


            <div class="my-2">
                <input type="text" name="firstname" readonly aria-label="First name" class="form-control" placeholder="First Name" value="{{ $userdata->admin_fname }}">
            </div>

            <div class="my-2">
                <input type="text" name="lastname" readonly aria-label="Last Name" class="form-control" placeholder="Last Name" value="{{ $userdata->admin_lname }}">
            </div>

            <div class="my-2">
                <input type="text" name="username" readonly aria-label="Username" class="form-control" placeholder="Username" value="{{ $userdata->admin_username }}">
            </div>

            <div class="my-2">
                <input type="password" name="password" readonly aria-label="Password" class="form-control" placeholder="Password" value="{{ $userdata->admin_password }}">
            </div>

            <div class="my-2">
                <input type="text" name="email" readonly aria-label="Email" class="form-control" placeholder="Email" value="{{ $userdata->admin_email }}">
            </div>

            <div class="my-2">
                <input type="text" name="phone" readonly aria-label="Contact" class="form-control" placeholder="Contact" value="{{ $userdata->admin_contact }}">
            </div>


    
        <a href="/logout" type="button" class="btn btn-link btn-sm text-decoration-none float-end mt-2">Logout</a>

    </div>
       


  </div>







    
</body>
</html>