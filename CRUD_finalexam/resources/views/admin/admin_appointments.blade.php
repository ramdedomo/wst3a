<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

</head>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>

<script>
$(document).ready(function () {
    $('#example').DataTable();
});

$(document).ready(function () {
    $('#example2').DataTable();
});
</script>


<body class="bg-dark">

  <div class="container">

    <span class="badge bg-primary mt-5">ADMINISTRATOR</span>

      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/admin">Products</a>
          </li>
          <li class="nav-item bg-primary rounded m-2">
            <a class="nav-link text-light" href="/adminappointments">Orders & Users</a>
          </li>
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/adminprofile">Profile</a>
          </li>

        </ul>

          <hr class="mt-3 mx-2" style="color: white">
      </div>


      <h1 class="mb-3 text-light">Orders</h1>
      <div class="container bg-light p-2 rounded">


        <table id="example" class="display" style="width:100%">
          <thead>
              <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Ordered Product</th>
                  <th>Date Purchased</th>
                  <th>Quantity</th>
                  <th>Total Price</th>
              </tr>
          </thead>

          @php
            $totalprice = 0;
          @endphp

          <tbody>

            @foreach ($your_appointmentData as $a)
            
            <tr>
              <td>{{ $a->firstname }} </td>
              <td>{{ $a->lastname }} </td>
              <td>{{ $a->admin_appointment_name }} </td>
              <td>{{ $a->date_puchased }} </td>
              <td>{{ $a->qty }} </td>
              <td>{{ $a->price * $a->qty  }}.00 PHP</td>
            </tr>

            @php
              $totalprice += $a->price * $a->qty;
            @endphp

            @endforeach  

          </tbody>
      </table>
      </div>

      <div class="float-end text-light mt-3">
        @php
        echo "Total Price: " . $totalprice . ".00 PHP";
        @endphp
      </div>

      <hr class="text-light">

      <h1 class="mb-3 text-light mt-5">Users</h1>
      <div class="container bg-light p-2 rounded mb-5">


        <table id="example2" class="display" style="width:100%">
          <thead>
              <tr>
                  <th>Firstname</th>
                  <th>Lastname</th>
                  <th>Email</th>
                  <th>Phone</th>
                  <th>Username</th>
              </tr>
          </thead>

          <tbody>

            @foreach ($user as $a)
            
            <tr>
              <td>{{ $a->firstname }} </td>
              <td>{{ $a->lastname }} </td>
              <td>{{ $a->email }} </td>
              <td>{{ $a->phone }} </td>
              <td>{{ $a->username  }}</td>
            </tr>

            @endforeach  

          </tbody>
      </table>
    
      </div>

      {{-- <a href="logout" class="btn btn-primary">Logout</a> --}}

  </div>



    
</body>
</html>