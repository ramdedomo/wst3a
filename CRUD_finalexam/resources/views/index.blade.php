<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</head>
<body class="bg-dark">

  <div class="container">

    <span class="badge bg-success mt-5">USER</span>

      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          <li class="nav-item bg-light rounded m-2">
            <a class="nav-link text-dark" href="/index">Products</a>
          </li>

          <li class="nav-item bg-light rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/profile/{{ $sessionUserInfo['user_id'] }}">Profile</a>
          </li>

        </ul>

          <hr class="mt-3 mx-2" style="color: white">
      </div>


      <div class="container">

        <div class="row">

          @foreach ($appointmentData as $appData)

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <div class="card" style="height: 250px">

              @if ($appData->stocks == 0)
              <div class="card-header bg-danger text-light">
                Stocks: Out of Stocks
                @else
                <div class="card-header bg-success text-light">
                Stocks: {{ $appData->stocks }}
                @endif

              </div>
              <div class="card-body">
                <h5 class="card-title">{{ $appData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $appData->admin_appointment_desc }}</p>

                @if ($appData->stocks == 0)
         
                  @else
                <a href="/order/{{ $appData->admin_appointment_id }}" class="btn btn-dark position-absolute bottom-0 end-0 m-3">Buy Now</a>
                  @endif
 
              </div>
            </div>
          </div>

          @endforeach

        </div>



        {{-- {{ $sessionValue }}

        {{ $sessionUserInfo['firstname'] }}
        {{ $sessionUserInfo['lastname'] }} --}}
      </div>

      {{-- <a href="logout" class="btn btn-primary">Logout</a> --}}

  </div>







    
</body>
</html>