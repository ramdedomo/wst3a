<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
    
</head>
<body class="bg-dark">

  <div class="container">

    <span class="badge bg-success mt-5">USER</span>

      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          <li class="nav-item bg-light rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/index">Appointments</a>
          </li>
          <li class="nav-item bg-light rounded m-2">
            <a class="nav-link text-dark" href="/yourappointments">Your Appointments</a>
          </li>
          <li class="nav-item bg-light rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/profile/{{ $sessionUserInfo['user_id'] }}">Profile</a>
          </li>
        </ul>

          <hr class="mt-3 mx-2" style="color: white">
      </div>



      <div class="container">

            @if(Session::get('success'))
                <div class="alert alert-success">
                {{ Session::get('success') }}
                </div>
            @endif

        <div class="row">

          @if($your_appointmentData->isEmpty())
          <div class="p-3 text-dark bg-opacity-10 bg-light col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2 position-absolute top-50 start-50 translate-middle rounded">
            <div class="text-center text-light" style="font-size: 10px">
              WOW, SUCH EMPTY
            </div>
            
          </div>
          @else

          @foreach ($your_appointmentData as $yourappData)

          @if ($yourappData->status == 1)

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-bg-primary my-2">Approved</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text"><span class="fw-bold">{{ $yourappData->appointment_time . " / " . $yourappData->appointment_date }}</span></p>

                <a onclick="return  confirm('Are you sure to cancel this Appointment?')" href="/cancel/{{ $yourappData->appointment_id }}" class="text-light btn btn-danger stretched-link position-absolute bottom-0 end-0 m-3">Cancel Appointment</i></a>
              </div>
            </div>
          </div>

          @elseif ($yourappData->status == 0)

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-bg-secondary my-2">Pending</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text"><span class="fw-bold">{{ $yourappData->appointment_time . " / " . $yourappData->appointment_date }}</span></p>

   
                <a onclick="return  confirm('Are you sure to delete this Appointment?')" href="/delete/{{ $yourappData->appointment_id }}" class="text-light btn btn-danger stretched-link position-absolute bottom-0 end-0 m-3"><i class="bi bi-trash3-fill"></i></a>
              </div>
            </div>
          </div>

          @elseif ($yourappData->status == 2)

          
          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-bg-danger my-2">Occupied</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text"><span class="fw-bold">{{ $yourappData->appointment_time . " / " . $yourappData->appointment_date }}</span></p>
                <a onclick="return  confirm('Are you sure to delete this Appointment?')" href="/delete/{{ $yourappData->appointment_id }}" class="text-light btn btn-danger stretched-link position-absolute bottom-0 end-0 m-3"><i class="bi bi-trash3-fill"></i></a>
              </div>
            </div>
          </div>

          @else

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-bg-danger my-2">Canceled</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text"><span class="fw-bold">{{ $yourappData->appointment_time . " / " . $yourappData->appointment_date }}</span></p>
                <a onclick="return  confirm('Are you sure to delete this Appointment?')" href="/delete/{{ $yourappData->appointment_id }}" class="text-light btn btn-danger stretched-link position-absolute bottom-0 end-0 m-3"><i class="bi bi-trash3-fill"></i></a>
              </div>
            </div>
          </div>



          @endif

          @endforeach
              
          @endif


        

        </div>



        {{-- {{ $sessionValue }}
        {{ $sessionUserInfo['user_id'] }}
        {{ $sessionUserInfo['firstname'] }}
        {{ $sessionUserInfo['lastname'] }} --}}
      </div>

      {{-- <a href="logout" class="btn btn-primary">Logout</a> --}}

  </div>







    
</body>
</html>