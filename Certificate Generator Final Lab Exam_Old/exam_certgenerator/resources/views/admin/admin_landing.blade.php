<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
  body{
    zoom: 110%;
  }
</style>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      <body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/admin">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">



                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/admin">Home</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/admin/editing">Edit Designs</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/admin/seminars">Edit Seminars</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/generatedcertificates">Generated Certificates</a>
                  </li>

                  <li class="nav-item">
                    <a href="/logout" type="button" class="p-0 m-0 btn btn-link btn-sm text-decoration-none mt-4">Logout</a>
                  </li>


                </ul>
              </div>
            </div>
          </nav>



    </div>

    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="badge text-bg-warning text-light">Logged as Admin</span>
            </div>
          
          </div>
    </div>


    <div class="container px-4 mt-4">

      @if(Session::get('success'))
      <div class="alert alert-success">
      {{ Session::get('success') }}
      </div>
      @endif

      @if(Session::get('fail'))
          <div class="alert alert-secondary">
          {{ Session::get('fail') }}
          </div>
      @endif

      <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-scrollable">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Add Seminar</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">

              <form action="/insertseminar" method="post">
                @csrf

              <span style="font-size: 10px" class="text-danger">
                @error('seminar'){{ $message }}
                
                <script>
                  $(function() {
                      $('#exampleModal').modal('show');
                  });
                </script>
                
                @enderror
              </span>

              <div class="mb-1" style="font-size: 15px;">Seminar Name:</div>
              <input placeholder="Seminar Name" name="seminar" type="text" class="form-control col-6">
           
              <span style="font-size: 10px" class="text-danger">
                @error('seminardesc'){{ $message }}
                
                <script>
                  $(function() {
                      $('#exampleModal').modal('show');
                  });
                </script>
                
                @enderror
              </span>

              <div class="mt-3 mb-1" style="font-size: 15px;">Seminar Description:</div>
              <textarea placeholder="Seminar Description" name="seminardesc" type="text" class="form-control col-6"></textarea>

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
            </div>

          </form>

          </div>
        </div>
      </div>

      <div class="modal fade" id="newModules" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New Modules</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">

              <form action="/insertModules" method="post" enctype="multipart/form-data">
                @csrf

                <span style="font-size: 10px" class="text-danger">
                  @error('type'){{ $message }}
                  
                  <script>
                    $(function() {
                        $('#newModules').modal('show');
                    });
                  </script>
                  
                  @enderror
                </span>
  
                <div class="mb-1" style="font-size: 15px;">Type:</div>
                <select name="type" id="type" class="form-control">
                  <option value="1">Logo</option>
                  <option value="2">Signature</option>
                  <option value="3">Base Image</option>
                </select>

                <div id="baseimage_layout">
                  <div class="mt-3 mb-1" style="font-size: 15px;">Example Size and Layout: (2000 x 1400)</div>
                  <img class="rounded shadow" width="100%" src="{{ asset('images/Layout.png') }}" alt="">
                </div>

                <div id="signature_layout">
                  <div class="mt-3 mb-1" style="font-size: 15px;">Signature Size: <b>(1280 x 500) PNG</b></div>
                </div>

                <div class="mt-3 mb-1" style="font-size: 15px;">Name:</div>
                <input name="name" type="text" class="form-control col-6">

                <div class="mt-3 mb-1" id="pos" style="font-size: 15px;">Position:</div>
                <input name="position" id="posinput" type="text" class="form-control col-6">

           
              <span style="font-size: 10px" class="text-danger">
                @error('file'){{ $message }}
                
                <script>
                  $(function() {
                      $('#newModules').modal('show');
                  });
                </script>
                
                @enderror
              </span>

              <div class="mt-3 mb-1" style="font-size: 15px;">Upload Photo:</div>
              <input name="file" type="file" class="form-control col-6">

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
            </div>

          </form>

          </div>
        </div>
      </div>


      <div class="modal fade" id="exampleModal3" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">Generate Certificates</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>


            <div class="modal-body">

                
              <form action="insertGeneratedCerts" method="post" enctype="multipart/form-data">
                @csrf
              <div class="row">
                <div class="col-6">
                  <div class="mb-1" style="font-size: 15px;">Method:</div>
                  <select name="method" id="method" class="form-control mb-3">
                    <option value="0">Manualy</option>
                    <option value="1">Import CSV</option>
                  </select>
                </div>

                <div class="col-6">
                  <div class="mb-1" style="font-size: 15px;">Seminar:</div>
    
                    <span style="font-size: 10px" class="text-danger">
                      @error('seminar'){{ $message }} 
    
                        <script>
                          $(function() {
                              $('#exampleModal3').modal('show');
                          });
                        </script>
    
                      @enderror
                    </span>
    
                  <select name="seminar" id="seminar" class="form-control mb-3">
    
                    @foreach ($seminars as $sem)
                    <option value="{{ $sem->seminar_id }}">{{ $sem->seminar_name }}</option>
                    @endforeach
    
                  </select>
  
                </div>
              </div>
          
              <hr>


              <div class="row mb-1">
                <div class="col-6">
                  <span style="font-size: 10px" class="text-danger">
                    @error('firstname'){{ $message }}
                    
                    <script>
                      $(function() {
                          $('#exampleModal3').modal('show');
                      });
                    </script>
                    
                    @enderror
                  </span>
    
                  <div class="mb-1" id="firstname_label" style="font-size: 15px;">First Name:</div>
                  <input placeholder="Firstname" name="firstname" id="firstname" type="text" class="form-control col-6">
                  </div>
    
                  <div class="col-6">
                    <span style="font-size: 10px" class="text-danger">
                      @error('lastname'){{ $message }} 
      
                      <script>
                        $(function() {
                            $('#exampleModal3').modal('show');
                        });
                      </script>
      
                      @enderror
                    </span>
      
                    <div class="mb-1" id="lastname_label" style="font-size: 15px;">Last Name:</div>
                    <input placeholder="Lastname" name="lastname" id="lastname" type="text" class="form-control col-6">
                  </div>
              </div>


              <span style="font-size: 10px" class="text-danger">
                @error('email'){{ $message }} 

                <script>
                  $(function() {
                      $('#exampleModal3').modal('show');
                  });
                </script>

                @enderror
              </span>

              <div class="mb-1" id="email_label" style="font-size: 15px;">Email:</div>
              <input name="email" id="email" type="email" placeholder="example@gmail.com" class="form-control col-12">


              <div class="mb-1" id="file_excel_label" style="font-size: 15px;">Emport File:</div>
              <input name="file_excel" id="file_excel" type="file" class="form-control col-12">
    
            </div>



            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Submit</button>
            </div>
           
          </form>

          </div>
        </div>
      </div>



      <div class="row rounded shadow">

        <div class="col-4 p-2">
          <div class="card text-center shadow zoom text-light p-3">
            <div class="card-body">
              <h5 class="card-title">New Seminars</h5>
              <a data-bs-toggle="modal" data-bs-target="#exampleModal"  class="stretched-link"></a>
            </div>
          </div>
        </div>

        <div class="col-4 p-2">
          <div class="card text-center shadow zoom text-light p-3">
            <div class="card-body">
       
              <h5 class="card-title">Design Modules</h5>
              <a data-bs-toggle="modal" style="background-color: white" data-bs-target="#newModules"  class="stretched-link"></a>
            </div>
          </div>
        </div>

        
        <div class="col-4 p-2">
          <div class="card text-center shadow zoom text-light p-3">
            <div class="card-body">
              <h5 class="card-title">Generate Certificates</h5>
              <a data-bs-toggle="modal" data-bs-target="#exampleModal3" href="/generatedcertificates" class="stretched-link"></a>
            </div>
          </div>
        </div>

        <div class="col-4 p-2">
          <div class="card text-center shadow zoom text-light p-3">
            <div class="card-body">
              <h5 class="card-title">List of Seminars</h5>
              <a href="/admin/seminars" class="stretched-link"></a>
            </div>
          </div>
        </div>

        <div class="col-4 p-2">
          <div class="card text-center shadow zoom text-light p-3">
            <div class="card-body">
              <h5 class="card-title">List of Design Certificates</h5>
              <a href="/admin/editing" class="stretched-link"></a>
            </div>
          </div>
        </div>

        <div class="col-4 p-2">
          <div class="card text-center shadow text-light zoom p-3">
            <div class="card-body">
              <h5 class="card-title">List of Generated Certificates</h5>
              <a href="/generatedcertificates" class="stretched-link"></a>
            </div>
          </div>
        </div>

      </div>


  </div>



    
</body>
</html>

<script>
  $( document ).ready(function() {

    if($( "#method option:selected" ).val() != 1){
          $("#firstname").show();
          $("#lastname").show();
          $("#lastname_label").show();
          $("#firstname_label").show();

          $("#email").show();
          $("#email_label").show();

          $("#file_excel_label").hide();
          $("#file_excel").hide();
      }else{
        $("#firstname").hide();
          $("#lastname").hide();
          $("#lastname_label").hide();
          $("#firstname_label").hide();

          $("#email").hide();
          $("#email_label").hide();
          
          $("#file_excel_label").show();
          $("#file_excel").show();
     }

     $( "#method" ).change(function() {

      if($( "#method option:selected" ).val() != 1){
          $("#firstname").show();
          $("#lastname").show();
          $("#lastname_label").show();
          $("#firstname_label").show();

          $("#email").show();
          $("#email_label").show();

          $("#file_excel_label").hide();
          $("#file_excel").hide();
      }else{
        $("#firstname").hide();
          $("#lastname").hide();
          $("#lastname_label").hide();
          $("#firstname_label").hide();

          $("#email").hide();
          $("#email_label").hide();
          
          $("#file_excel_label").show();
          $("#file_excel").show();
     }

      });




      if($( "#type option:selected" ).val() == 2){
          $("#pos").show();
          $("#posinput").show(); 
          $("#baseimage_layout").hide();
          $("#signature_layout").show();
          
      }else if($( "#type option:selected" ).val() == 1){
          $("#pos").hide();
          $("#posinput").hide();
          $("#baseimage_layout").hide();    
          $("#signature_layout").hide();
      } else{
          $("#baseimage_layout").show();
          $("#pos").hide();
          $("#posinput").hide();
          $("#signature_layout").hide();
      }
      


      $( "#type" ).change(function() {
      console.log("asd");
      console.log($( "#type option:selected" ).text());
      console.log($( "#type option:selected" ).val());

      if($( "#type option:selected" ).val() == 2){
          $("#pos").show();
          $("#posinput").show(); 
          $("#baseimage_layout").hide();
          $("#signature_layout").show();
          
      }else if($( "#type option:selected" ).val() == 1){
          $("#pos").hide();
          $("#posinput").hide();
          $("#baseimage_layout").hide();    
          $("#signature_layout").hide();
      } else{
          $("#baseimage_layout").show();
          $("#pos").hide();
          $("#posinput").hide();
          $("#signature_layout").hide();
      }
      


      });

      
  });
</script>




<style>



  .zoom{
    background-color: #002fa5;
    
  }

  .zoom:hover {
  background-color: #003cd4;
  transform: scale(1.01);
  transition: all 0.4s ease;/* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
  }


</style>


