<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

<?php
 
session_start();

//if not now is click or cancel
if(!isset($_GET['code'])){
  if ($_GET['error_code'] == 200){
    header('Location: index.php');
}
}

if(!isset($_SESSION['fbname'])){
    header('Location: index.php');
}

require_once('php-graph-sdk-5.x/src/Facebook/autoload.php');
 
$fb = new Facebook\Facebook([
  'app_id' => '353124420052682',
  'app_secret' => '56555d7da11042ce6374f9decfefd4f8',
  'default_graph_version' => 'v13.0',
]);  
  
$helper = $fb->getRedirectLoginHelper();  
  
try 
{  
  $accessToken = $helper->getAccessToken();  
} catch(Facebook\Exceptions\FacebookResponseException $e) {  
  // When Graph returns an error  
  
  echo 'Graph returned an error: ' . $e->getMessage();  
  exit;  
} catch(Facebook\Exceptions\FacebookSDKException $e) {  
  // When validation fails or other local issues  
 
  echo 'Facebook SDK returned an error: ' . $e->getMessage();  
  exit;  
}  

 
try 
{
  $response = $fb->get('/me?fields=id,name,email,first_name,last_name', $accessToken->getValue());

} catch(Facebook\Exceptions\FacebookResponseException $e) {
  // When Graph returns an error
  echo 'ERROR: Graph ' . $e->getMessage();
  exit;
} catch(Facebook\Exceptions\FacebookSDKException $e) {
  // When validation fails or other local issues
  echo 'ERROR: validation fails ' . $e->getMessage();
  exit;
}
 
$me = $response->getGraphUser();
 

$_SESSION['fbname'] = $me->getProperty('name');

header("Location: fbdisplay.php")


 
?>