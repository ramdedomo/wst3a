<?php
require_once('googlelogin.php');

if(isset($_SESSION['fbname'])){
    header('Location: fblogin.php');
}

if(isset($_SESSION['googlename'])){
    header('Location: googledisplay.php');
}
?>

<html>
<head>
	<title>Activity Login</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.3.0/font/bootstrap-icons.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
</head>
<body>

    <div class="text-center container mt-5">
    <b>LOGIN ACTIVITY</b>

    <table align="center" class="mt-3 mb-3" style="width: 25%;">
        <tr>
        <td>
        Ram Wendel Dedomo
        </td>

        <td>
        BSIT - 3A
        </td>
        </tr>


        <tr>
        <td>
        <?php echo date('F d,Y'); ?>
        </td>


        <td>
        <?php
        date_default_timezone_set("Asia/Manila");
        echo date("h:i a");
        ?>
        </td>
        </tr> 
    </table>

    <hr>
            <div class="mt-3">         
                <div style="margin-top:10px" class="form-group">
                    <div class="col-sm-12 controls">
                         <a href="fblogin.php" class="btn btn-primary"><i class="img-fluid bi bi-facebook p-2"></i>Login with Facebook</a>
                         <?php echo "<a class='btn btn-secondary' href='".$client->createAuthUrl()."'><i class='img-fluid bi bi-google p-2'></i>Login with Google</a>"; ?>
                    </div>
                </div>                    
            </div>
    </div>

</body>
</html>