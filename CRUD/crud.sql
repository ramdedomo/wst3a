-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 23, 2022 at 02:36 AM
-- Server version: 10.4.22-MariaDB
-- PHP Version: 8.1.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `administrator`
--

CREATE TABLE `administrator` (
  `admin_password` varchar(50) DEFAULT NULL,
  `admin_email` varchar(50) DEFAULT NULL,
  `admin_contact` int(11) DEFAULT NULL,
  `admin_fname` varchar(50) DEFAULT NULL,
  `admin_lname` varchar(50) DEFAULT NULL,
  `admin_id` int(11) NOT NULL,
  `admin_username` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `administrator`
--

INSERT INTO `administrator` (`admin_password`, `admin_email`, `admin_contact`, `admin_fname`, `admin_lname`, `admin_id`, `admin_username`) VALUES
('12345', 'admin@email.com', 2147483647, 'Juan', 'Cruz', 1, 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `admin_appointments`
--

CREATE TABLE `admin_appointments` (
  `admin_appointment_id` int(11) NOT NULL,
  `admin_appointment_name` varchar(50) DEFAULT NULL,
  `admin_appointment_desc` text DEFAULT NULL,
  `admin_appointment_featured` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `admin_appointments`
--

INSERT INTO `admin_appointments` (`admin_appointment_id`, `admin_appointment_name`, `admin_appointment_desc`, `admin_appointment_featured`, `admin_id`) VALUES
(1, 'Regular Bath', 'Includes a bath, blow dry, and brush out', 1, 1),
(3, 'Full Bath', 'Includes a bath, Blow dry, Brush out, Nail trim, Ear Cleaning and Anal Gland expression.', 1, 1),
(4, 'Full Groom', 'Includes Bath the Works and a haircut of your liking', 1, 1),
(5, 'Tooth Brushing', 'Monitor your dog\'s vital signs and use special tools to polish the teeth and remove plaque and tartar from underneath the gums.', 0, 1),
(6, 'De shedding', 'Protect your pet from unexpected and painful mats. Avoid hidden hot spots! De-shedding helps prevent hot spots by drawing out natural oils released by a dog\'s skin and fur.', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `appointments`
--

CREATE TABLE `appointments` (
  `appointment_id` int(11) NOT NULL,
  `appointment_time` varchar(50) DEFAULT NULL,
  `appointment_date` varchar(50) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `admin_id` int(11) DEFAULT NULL,
  `admin_appointment_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `appointments`
--

INSERT INTO `appointments` (`appointment_id`, `appointment_time`, `appointment_date`, `user_id`, `admin_id`, `admin_appointment_id`, `status`) VALUES
(83, '8-10am', '2022-06-01', 10, 1, 1, 1),
(85, '1-3pm', '2022-06-01', 10, 1, 1, 1),
(86, '8-10am', '2022-06-02', 10, 1, 1, 3),
(87, '1-3pm', '2022-06-01', 10, 1, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL DEFAULT '',
  `firstname` varchar(50) DEFAULT NULL,
  `lastname` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `username`, `firstname`, `lastname`, `email`, `phone`, `password`) VALUES
(10, 'user1', 'Ram Wendel', 'Dedomo', 'ramdedomo@gmail.com', '091237182391', '12345');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `administrator`
--
ALTER TABLE `administrator`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `admin_appointments`
--
ALTER TABLE `admin_appointments`
  ADD PRIMARY KEY (`admin_appointment_id`) USING BTREE,
  ADD KEY `FK_admin_appointments_administrator` (`admin_id`);

--
-- Indexes for table `appointments`
--
ALTER TABLE `appointments`
  ADD PRIMARY KEY (`appointment_id`),
  ADD KEY `FK_appointments_user` (`user_id`),
  ADD KEY `FK_appointments_administrator` (`admin_id`),
  ADD KEY `FK_appointments_admin_appointments` (`admin_appointment_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `administrator`
--
ALTER TABLE `administrator`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `admin_appointments`
--
ALTER TABLE `admin_appointments`
  MODIFY `admin_appointment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `appointments`
--
ALTER TABLE `appointments`
  MODIFY `appointment_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=88;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `admin_appointments`
--
ALTER TABLE `admin_appointments`
  ADD CONSTRAINT `FK_admin_appointments_administrator` FOREIGN KEY (`admin_id`) REFERENCES `administrator` (`admin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `appointments`
--
ALTER TABLE `appointments`
  ADD CONSTRAINT `FK_appointments_admin_appointments` FOREIGN KEY (`admin_appointment_id`) REFERENCES `admin_appointments` (`admin_appointment_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_appointments_administrator` FOREIGN KEY (`admin_id`) REFERENCES `administrator` (`admin_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `FK_appointments_user` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
