<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdministratorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('administrator', function (Blueprint $table) {
            $table->string('admin_password', 50)->nullable();
            $table->string('admin_email', 50)->nullable();
            $table->integer('admin_contact')->nullable();
            $table->string('admin_fname', 50)->nullable();
            $table->string('admin_lname', 50)->nullable();
            $table->integer('admin_id', true);
            $table->string('admin_username', 50)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('administrator');
    }
}
