<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->integer('appointment_id', true);
            $table->string('appointment_time', 50)->nullable();
            $table->date('appointment_date')->nullable();
            $table->integer('user_id')->nullable()->index('FK_appointments_user');
            $table->integer('admin_id')->nullable()->index('FK_appointments_administrator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
