<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\AdminAppointments;
use App\Models\Administrator;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Appointments;
use Illuminate\Validation\Rule;


class CRUDdb extends Controller
{

    function addservices(){
        return view('addservices');
    }

    function confirmaddservices(Request $request){

        $request->validate([
            'servicename'=>'required',
            'servicedesc'=>'required',
            'tos'=>'required',
        ]);

        $insert = DB::table('admin_appointments')->insert([
            'admin_appointment_name' => $request->servicename,
            'admin_appointment_desc' => $request->servicedesc,
            'admin_appointment_featured' => $request->tos,
        ]);
            
            if($insert){
                return back()
                ->with('success','Service Added!');
            }else{
                 return back()->with('fail','Something went wrong, Try again later');
            }
        
    }

    function occupied($id){
        $update = DB::table('appointments')
              ->where('appointment_id', $id)
              ->update(['status' => 2]);

              return back();

    }

    function cancel($id){
        $update = DB::table('appointments')
              ->where('appointment_id', $id)
              ->update(['status' => 3]);

              return back();

    }

    function approved($id){
        $update = DB::table('appointments')
            ->where('appointment_id', $id)
            ->update(['status' => 1]);

            return back();
    }



    function search(Request $request){

            $id =   Administrator::where('admin_username','=', session('sessionUser'))->get('admin_id');
            $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

            $yourappData = DB::table('appointments')
            ->join('user', 'appointments.user_id', '=', 'user.user_id')
            ->join('admin_appointments',  'admin_appointments.admin_appointment_id', '=', 'appointments.admin_appointment_id')
            ->select("*")
            ->where('appointments.admin_id','=', $id[0]['admin_id'])
            ->where('appointments.appointment_date', 'LIKE', "%$request->search%")
            ->orWhere('appointments.appointment_time', 'LIKE', "%$request->search%")
            ->get();

            return view('admin.admin_appointments', ['status'=>'1'], ['your_appointmentData'=>$yourappData], ['sessionUserInfo'=>$data]);

    }

    function admin(){
        $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

        $appData = DB::table('admin_appointments')
        ->select("*")
        ->get();

        return view('admin.indexadmin', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);
    }

    function adminappointments(){

        $id =   Administrator::where('admin_username','=', session('sessionUser'))->get('admin_id');
        $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

        $yourappData = DB::table('appointments')
        ->join('user', 'appointments.user_id', '=', 'user.user_id')
        ->join('admin_appointments',  'admin_appointments.admin_appointment_id', '=', 'appointments.admin_appointment_id')
        ->select("*")
        ->where('appointments.admin_id','=', $id[0]['admin_id'])
        ->get();


        
        return view('admin.admin_appointments', ['status'=>'0'], ['your_appointmentData'=>$yourappData], ['sessionUserInfo'=>$data]);
    }

    function adminprofile(){
        $userdata = Administrator::where('admin_username','=', session('sessionUser'))->first();
        return view('admin.admin_profile', ['userdata'=>$userdata]);
    }
    
    function logout(){
        if(session()->has('sessionUser')){
            session()->pull('sessionType');
            session()->pull('sessionUser');
            return redirect('login');
        }
    }

    function login(){
        // $session = ['sessionValue'=>session('sessionUser')];
        return view('login');
    }

    function register(){
        return view('register');
    }


    function index(){
        $session = session('sessionUser');
        $data = User::where('username','=', session('sessionUser'))->first();

        $appData = DB::table('admin_appointments')
        ->select("*")
        ->get();

        return view('index', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);
    }

    function yourappointments(){
        
        $id = User::where('username','=', session('sessionUser'))->get('user_id');
        $data = User::where('username','=', session('sessionUser'))->first();

        $yourappData = DB::table('appointments')
        ->join('admin_appointments', 'appointments.admin_appointment_id', '=', 'admin_appointments.admin_appointment_id')
        ->select("*")
        ->where('appointments.user_id','=', $id[0]['user_id'])
        ->get();

        return view('yourappointments', ['your_appointmentData'=>$yourappData], ['sessionUserInfo'=>$data]);
    }

    function profile(){
        $userdata = User::where('username','=', session('sessionUser'))->first();
        return view('profile', ['userdata'=>$userdata]);
    }


    function save(Request $request){
        
        //Validate requests
        $request->validate([
            'firstname'=>'required',
            'lastname'=>'required',
            'phone'=>'required|numeric',
            'username'=>'required|unique:user',
            'email'=>'required|email|unique:user',
            'password'=>'required'
        ]);


         //  Insert data into database
         $user = new user;
         $user ->firstname = $request->firstname;
         $user ->lastname = $request->lastname;
         $user ->phone = $request->phone;
         $user ->username = $request->username;
         $user ->email = $request->email;
         $user ->password = $request->password;
         $insert = $user ->save();

         if($insert){
            return back()->with('success','Registered');
         }else{
             return back()->with('fail','Something went wrong, Try again later');
         }

    }

    function addappoint($id, Request $request){

            $appointmentSelected = AdminAppointments::where('admin_appointment_id','=', $id)->get();
            $user_id = User::where('username','=', session('sessionUser'))->get('user_id');

            $request->validate([
                'appointment_time'=>'required',
                'appointment_date'=>'required'
            ]);

            $appoint = Appointments::get();
            $occupiedcounter = 0;

                if(Appointments::count() != 0){
               
                foreach ($appoint as $appointment) {
                    if(($appointment->appointment_time == $request->appointment_time) && ($appointment->appointment_date == $request->appointment_date." 00:00:00")){
                        $occupiedcounter++;
                        
                        //if cancelled
                        if($appointment->status == 3){
                            $occupiedcounter--;
                        }
                    } 
                }

                if($occupiedcounter == 0){

                    $insert = DB::table('appointments')->insert([
                        'appointment_time' => $request->appointment_time,
                        'appointment_date' => $request->appointment_date,
                        'user_id' => $user_id[0]['user_id'],
                        'admin_id' => $appointmentSelected[0]['admin_id'],
                        'admin_appointment_id' => $id,
                        'status' => 1
                    ]);
                        
                        if($insert){
                            return back()
                            ->with('success','Click "Your Appointments" tab to see its status')
                            ->with('status','Status: Approved');
                        }else{
                             return back()->with('fail','Something went wrong, Try again later');
                        }
                    }else{

                            $insert = DB::table('appointments')->insert([
                                'appointment_time' => $request->appointment_time,
                                'appointment_date' => $request->appointment_date,
                                'user_id' => $user_id[0]['user_id'],
                                'admin_id' => $appointmentSelected[0]['admin_id'],
                                'admin_appointment_id' => $id,
                                'status' => 0
                            ]);
                                
                                if($insert){
                                     return back()
                                     ->with('success','Click "Your Appointments" tab to see its status')
                                     ->with('status','Status: Pending');
                                }else{
                                     return back()->with('fail','Something went wrong, Try again later');
                                }
                    }
    

                }
                    else{

                            $insert = DB::table('appointments')->insert([
                                'appointment_time' => $request->appointment_time,
                                'appointment_date' => $request->appointment_date,
                                'user_id' => $user_id[0]['user_id'],
                                'admin_id' => $appointmentSelected[0]['admin_id'],
                                'admin_appointment_id' => $id,
                                'status' => 1
                            ]);
                
                            if($insert){
                                return back()
                                     ->with('success','Click "Your Appointments" tab to see its status')
                                     ->with('status','Status: Approved');
                            }else{
                                return back()->with('fail','Something went wrong, Try again later');
                            }


                }

    }

    function home() {
        if(session('sessionType') == "user"){

            $session = session('sessionUser');
            $data = User::where('username','=', session('sessionUser'))->first();
    
            $appData = DB::table('admin_appointments')
            ->select("*")
            ->get();
    
            return view('index', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);

        }
        elseif(session('sessionType') == "admin"){

            $data = Administrator::where('admin_username','=', session('sessionUser'))->first();

            $appData = DB::table('admin_appointments')
            ->select("*")
            ->get();
    
            return view('admin.indexadmin', ['sessionUserInfo'=>$data], ['appointmentData'=>$appData]);

        }
        else{
            return view('login');
        }
    }


    function delete($id){
            $delete = DB::table('appointments')->where('appointment_id', '=', $id)->delete();

            if($delete){
                return back()->with('success','Appoinment Deleted!');
             }else{
                 return back()->with('fail','Something went wrong, Try again later');
             }
    }

    function deleteService($id){
        $delete = DB::table('admin_appointments')->where('admin_appointment_id', '=', $id)->delete();

        if($delete){
            return back()->with('success','Service Deleted!');
         }else{
             return back()->with('fail','Something went wrong, Try again later');
         }
}

    

    function appoint($id){

        $data = User::where('username','=', session('sessionUser'))->first();

        $appointmentSelected = DB::table('admin_appointments')
        ->select("*")
        ->where('admin_appointments.admin_appointment_id','=', $id)
        ->first();

        return view('appoint', ['appointment_Selected'=>$appointmentSelected], ['sessionUserInfo'=>$data]);
    }


    function check(Request $request){
        //Validate requests
        $request->validate([
             'username'=>'required',
             'password'=>'required'
        ]);
        
        $userInfo = User::where('username','=', $request->username)->first();
        //$userID = User::where('username','=', $request->username)->first(['user_id']);
        $adminInfo = Administrator::where('admin_username','=', $request->username)->first();

        if(!$userInfo){
            if(!$adminInfo){
            return back()->with('fail','Incorrect username or password');
            }else{
                if($request->password == $adminInfo->admin_password){
                    $request->session()->put('sessionType', 'admin');
                    $request->session()->put('sessionUser', $adminInfo->admin_username);
                    return redirect('admin');
                }else{
                    return back()->with('fail','Incorrect username or password');
                }
            }
        }else{

            //check password
            if($request->password == $userInfo->password){
                $request->session()->put('sessionType', 'user');
                $request->session()->put('sessionUser', $userInfo->username);
                return redirect('index');
            }else{
                return back()->with('fail','Incorrect username or password');
            }

        }

        
    }



}
