<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;


class checkSession
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {

        //admin

        //if session has value and the request path is login or register return to index
        // if(session()->has('sessionAdmin') && ($request->path() == 'login' || $request->path() == 'register' || $request->path() == 'index')){
        //     return redirect('admin');
        // }

        // // //if session has no value and the request path is not login or register return to login
        // if(!session()->has('sessionAdmin')&&
        // ($request->path() == 'admin'
        // )){
        //     return redirect('login')->with('fail','Admin, You must be logged in');
        // }

        //user

    
                            //if session has no value and the request path is not login or register return to login
        if(!session()->has('sessionUser') && ($request->path() !='login' && $request->path() !='register')){
                return redirect('login')->with('fail','You must be logged in');
        }

 
        $value = session('sessionType');



        if($value == "user"){

            //if session has value and the request path is login or register return to index
            if(session()->has('sessionUser') && ($request->path() == 'login' || $request->path() == 'register' || $request->path() == 'admin')){
                return redirect('index');
            }

        }

        elseif($value == "admin"){

            //if session has value and the request path is login or register return to index
            if(session()->has('sessionUser') && ($request->path() == 'login' || $request->path() == 'register' || $request->path() == 'index')){
                return redirect('admin');
            }

        }


        return $next($request);
    }
}
