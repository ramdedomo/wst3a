<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $user_id
 * @property int    $contact
 * @property string $Username
 * @property string $username
 * @property string $username
 * @property string $fname
 * @property string $lname
 * @property string $email
 * @property string $password
 * @property string $Password
 * @property string $User_Class
 * @property string $userpassword
 * @property string $email
 */
class User extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'user';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'user_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Username', 'username', 'username', 'fname', 'lname', 'email', 'contact', 'password', 'Password', 'User_Class', 'userpassword', 'email'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'user_id' => 'int', 'Username' => 'string', 'username' => 'string', 'username' => 'string', 'fname' => 'string', 'lname' => 'string', 'email' => 'string', 'contact' => 'int', 'password' => 'string', 'Password' => 'string', 'User_Class' => 'string', 'userpassword' => 'string', 'email' => 'string'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = false;

    // Scopes...

    // Functions ...

    // Relations ...
}
