<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $appointment_id
 * @property int    $user_id
 * @property int    $admin_id
 * @property int    $admin_appointment_id
 * @property int    $status
 * @property string $appointment_time
 * @property Date   $appointment_date
 */
class Appointments extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'appointments';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'appointment_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'appointment_time', 'appointment_date', 'user_id', 'admin_id', 'admin_appointment_id', 'status'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'appointment_id' => 'int', 'appointment_time' => 'string', 'appointment_date' => 'date', 'user_id' => 'int', 'admin_id' => 'int', 'admin_appointment_id' => 'int', 'status' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'appointment_date'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Scopes...

    // Functions ...

    // Relations ...
}
