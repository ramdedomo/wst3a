<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int    $admin_appointment_id
 * @property int    $admin_appointment_featured
 * @property int    $admin_id
 * @property string $admin_appointment_name
 * @property string $admin_appointment_desc
 */
class AdminAppointments extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'admin_appointments';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'admin_appointment_id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'admin_appointment_name', 'admin_appointment_desc', 'admin_appointment_featured', 'admin_id'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'admin_appointment_id' => 'int', 'admin_appointment_name' => 'string', 'admin_appointment_desc' => 'string', 'admin_appointment_featured' => 'int', 'admin_id' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var boolean
     */
    public $timestamps = true;

    // Scopes...

    // Functions ...

    // Relations ...
}
