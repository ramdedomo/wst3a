<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</head>
<body class="bg-dark">

    <div class="container">


        <div class="shadow p-3 bg-light text-dark col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2 position-absolute top-50 start-50 translate-middle rounded">
            <div class="fw-bold">REGISTER</div>
            <p style="font-size: 10px">GROOMING APPOINTMENT SYSTEM</p>
            <hr>

            @if(Session::get('success'))
                <div class="alert alert-success">
                {{ Session::get('success') }}
                </div>
            @endif

            @if(Session::get('fail'))
                <div class="alert alert-danger">
                {{ Session::get('fail') }}
                </div>
            @endif


            <form action="{{ route('save') }}" method="post">
                @csrf

                <span style="font-size: 10px" class="text-danger">@error('firstname'){{ $message }} @enderror</span>
                <div class="my-2">
                    <input type="text" name="firstname" aria-label="First name" class="form-control" placeholder="First Name">
                </div>
    
                <span style="font-size: 10px" class="text-danger">@error('lastname'){{ $message }} @enderror</span>
                <div class="my-2">
                    <input type="text" name="lastname" aria-label="Last Name" class="form-control" placeholder="Last Name">
                </div>
    
                <span style="font-size: 10px" class="text-danger">@error('username'){{ $message }} @enderror</span>
                <div class="my-2">
                    <input type="text" name="username" aria-label="Username" class="form-control" placeholder="Username">
                </div>
    
                <span style="font-size: 10px" class="text-danger">@error('password'){{ $message }} @enderror</span>
                <div class="my-2">
                    <input type="password" name="password" aria-label="Password" class="form-control" placeholder="Password">
                </div>
    
                <span style="font-size: 10px" class="text-danger">@error('email'){{ $message }} @enderror</span>
                <div class="my-2">
                    <input type="text" name="email" aria-label="Email" class="form-control" placeholder="Email">
                </div>
    
                <span style="font-size: 10px" class="text-danger">@error('phone'){{ $message }} @enderror</span>
                <div class="my-2">
                    <input type="text" name="phone" aria-label="Contact" class="form-control" placeholder="Contact">
                </div>
    
    
                <div class="d-grid gap-2 mt-3">
                    <button class="btn btn-primary btn-sm" type="submit">Register</button>
                </div>

            </form>
            
        
            <a href="/login" type="button" class="btn btn-link btn-sm text-decoration-none float-end mt-2">Login</a>

        </div>

    </div>


    
</body>
</html>