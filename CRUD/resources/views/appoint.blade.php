<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

</head>
<body class="bg-dark">

  <div class="container">

    <span class="badge bg-success mt-5">USER</span>

      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          <li class="nav-item bg-light rounded m-2 bg-opacity-10">
                <a class="nav-link text-light" href="/index">Appointments</a>
          </li>
          <li class="nav-item bg-light rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/yourappointments">Your Appointments</a>
          </li>
          <li class="nav-item bg-light rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/profile/{{ $sessionUserInfo['user_id'] }}">Profile</a>
          </li>
        </ul>

          <hr class="mt-3 mx-2" style="color: white">
      </div>


      <div class="container">

        <div class="shadow p-3 bg-light text-dark col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2 position-absolute top-50 start-50 translate-middle rounded">
            <div class="fw-bold">{{ $appointment_Selected->admin_appointment_name }}</div>
            <p style="font-size: 10px">{{ $appointment_Selected->admin_appointment_desc }}</p>
            <hr>

            @if(Session::get('success'))
                <div class="alert alert-secondary">
                {{ Session::get('status') }}
                </div>

                <div class="alert alert-success">
                {{ Session::get('success') }}
                </div>
            @endif

            @if(Session::get('fail'))
                <div class="alert alert-danger">
                {{ Session::get('fail') }}
                </div>
            @endif


            <form action="/addappoint/{{ $appointment_Selected->admin_appointment_id ?? 'None'}}" method="post">
                @csrf

                <span style="font-size: 10px" class="text-danger">@error('appointment_time'){{ $message }} @enderror</span>

                
                <div class="my-2">
                  <div style="font-size: 10px" class="fw-bold py-1">Appointment Time:</div>
                  <select class="form-select" name="appointment_time" aria-label="Default select example">
                    <option value="8-10am">8-10 am</option>
                    <option value="1-3pm">1-3 pm</option>
                    <option value="3-5pm">3-5 pm</option>
                    <option value="5-7pm">5-7 pm</option>
                  </select>
                </div>


                <span style="font-size: 10px" class="text-danger">@error('appointment_date'){{ $message }} @enderror</span>
                <div style="font-size: 10px" class="fw-bold py-1">Appointment Date:</div>
                <div>
                    <input type="date" name="appointment_date" aria-label="First name" class="form-control" placeholder="Date">
                </div>
                
    
                <div class="d-grid gap-2 mt-3">
                    <button class="btn btn-primary btn-sm" type="submit">Request Appointment</button>
                </div>

            </form>
      
        </div>

      </div>

      {{-- <a href="logout" class="btn btn-primary">Logout</a> --}}

  </div>







    
</body>
</html>