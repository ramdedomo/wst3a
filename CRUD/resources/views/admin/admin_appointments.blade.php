<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

</head>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>




<body class="bg-dark">

  <div class="container">

    <span class="badge bg-primary mt-5">ADMINISTRATOR</span>

      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/admin">Services</a>
          </li>
          <li class="nav-item bg-primary rounded m-2">
            <a class="nav-link text-light" href="/adminappointments">Your Appointments</a>
          </li>
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/adminprofile">Profile</a>
          </li>
        </ul>

          <hr class="mt-3 mx-2" style="color: white">
      </div>


      <div class="container">

        <div class="row">
          <div class="col-8">
          </div>
          <div class="col-4">
            <form action="{{ route('search') }}" class="d-flex" role="search">
              @csrf
              <input class="form-control me-2" name="search" type="search" placeholder="Search Date or Time" aria-label="Search">
              <button class="btn btn-primary me-2"  type="submit">Search</button>
              <a href="adminappointments" class="btn btn-warning text-light" type="submit">Reset</a>
            </form>
          </div>
        </div>

        <div class="mt-3 bg-light p-3 rounded" id="searchContainer">
          <table class="table table-light table-striped">
            <thead>
              <tr>
                <th scope="col">Appointment Time</th>
                <th scope="col">Appointment Date</th>
                <th scope="col">Service</th>
                <th scope="col">Client</th>
                <th scope="col">Action</th>
              </tr>
            </thead>
            <tbody id="tableBody">


                @if ($status == "0")

                

                @else

                  @foreach ($your_appointmentData as $yourappData)
                  <tr>
                    <th>{{ $yourappData->appointment_time }}</th>
                    <td>{{ $yourappData->appointment_date }}</td>
                    <td>{{ $yourappData->admin_appointment_name }}</td>
                    <td>{{ $yourappData->firstname }} {{ $yourappData->lastname }}</td>

                    @if ( $yourappData->status == 1 )
                    <td><span class="badge bg-primary my-2">Approved</span></td>
                    @elseif( $yourappData->status == 0 )
                    <td class="col-sm-4 col-md-2 col-lg-2 col-xl-1">
                      <a href="/occupied/{{ $yourappData->appointment_id }}" type="button" class="btn btn-warning btn-sm text-light"><i class="bi bi-x"></i></a>
                      <a href="/approved/{{ $yourappData->appointment_id }}" type="button" class="btn btn-primary btn-sm"><i class="bi bi-check2"></i></a>
                    </td>
                    @elseif( $yourappData->status == 2 )
                    <td><span class="badge bg-danger my-2">Occupied</span></td>

                    @else
                    <td><span class="badge bg-danger my-2">Canceled</span></td>

                    @endif

                  </tr>
               @endforeach

                



                @endif






            </tbody>
          </table>
        </div>

        <hr class="text-light">


        <div class="text-light rounded mb-3 mt-2 text-center">LIST OF <b>APPROVED</b> APPOINTMENTS</div>

        <div class="row bg-light bg-opacity-10 p-3 rounded">

          @foreach ($your_appointmentData as $yourappData)

          @if ($yourappData->status == 1)

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge bg-primary my-2">Approved</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>
                <p class="card-text">Client Name: <b>{{ $yourappData->firstname . " " .  $yourappData->lastname }}</b></p>
                
                <hr>
                <p class="card-text">at <span class="fw-bold">{{ $yourappData->appointment_date . " at " .  $yourappData->appointment_time }}</span></p>
              </div>
            </div>
          </div>
          @endif

          @endforeach
          


        </div>

        <hr style="color: white;">

        <div class="text-light rounded mb-3 mt-2 text-center">LIST OF <b>PENDING, OCCUPIED and CANCELED BY USER</b> APPOINTMENTS</div>

        <div class="row bg-light bg-opacity-10 p-3 rounded">



          @foreach ($your_appointmentData as $yourappData)
          @if ($yourappData->status == 1)

          @elseif ($yourappData->status == 0)

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-light bg-secondary my-2 shadow-sm">Pending</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text">at <span class="fw-bold">{{  $yourappData->appointment_date . " / " . $yourappData->appointment_time }}</span></p>

                <div class="position-absolute bottom-0 end-0 m-3">
                  
                  <a href="/occupied/{{ $yourappData->appointment_id }}" type="button" class="btn btn-warning btn-sm text-light">Occupied</a>
                  <a href="/approved/{{ $yourappData->appointment_id }}" class="btn btn-primary btn-sm">Approved</a>
                </div>
              </div>
            </div>
          </div>

          @elseif ($yourappData->status == 2)

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-light bg-danger my-2 shadow-sm">Occupied</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text">at <span class="fw-bold">{{  $yourappData->appointment_date . " / " . $yourappData->appointment_time }}</span></p>

                <div class="position-absolute bottom-0 end-0 m-3">
                </div>
              </div>
            </div>
          </div>

          @else

          <div class="col-12 col-sm-12 col-md-6 col-lg-4 col-xl-4 p-2">
            <span class="badge text-light bg-danger my-2 shadow-sm">Canceled by the User</span>
            <div class="card" style="height: 250px">
              <div class="card-body">
                <h5 class="card-title">{{ $yourappData->admin_appointment_name }}</h5>
                <p class="card-text">{{ $yourappData->admin_appointment_desc }}</p>

                <hr>
                <p class="card-text">at <span class="fw-bold">{{  $yourappData->appointment_date . " / " . $yourappData->appointment_time }}</span></p>

                <div class="position-absolute bottom-0 end-0 m-3">
                </div>
              </div>
            </div>
          </div>

          @endif

          @endforeach
              



        

        </div>



        {{-- {{ $sessionValue }}

        {{ $sessionUserInfo['firstname'] }}
        {{ $sessionUserInfo['lastname'] }} --}}
      </div>

      {{-- <a href="logout" class="btn btn-primary">Logout</a> --}}

  </div>







    
</body>
</html>