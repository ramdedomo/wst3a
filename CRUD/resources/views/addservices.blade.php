<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <!-- CSS only -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <!-- JavaScript Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">

  </head>
<body class="bg-dark">



  <div class="container">

    <span class="badge bg-primary mt-5">ADMINISTRATOR</span>
      <div>
        <ul class="nav nav-pills justify-content-end mt-5 px-2">
          <li class="nav-item bg-primary rounded m-2">
            <a class="nav-link text-light" href="/admin">Services</a>
          </li>
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/adminappointments">Your Appointments</a>
          </li>
          <li class="nav-item bg-primary rounded m-2 bg-opacity-10">
            <a class="nav-link text-light" href="/adminprofile">Profile</a>
          </li>
        </ul>

          <hr class="mt-3 mx-2" style="color: white">
          
      </div>



      <div class="container">

        <div class="mb-3">
          <a href="admin" class="btn btn-primary" type="button"><i class="bi bi-arrow-left-short"></i></i>GO BACK</a>
        </div>

        <div class="shadow p-3 bg-light text-dark col-6 col-sm-5 col-md-4 col-lg-3 col-xl-2 position-absolute top-50 start-50 translate-middle rounded">
          <div class="fw-bold">Add Service</div>
          <p style="font-size: 10px">APPOINTMENT SYSTEM</p>
          <hr>

          @if(Session::get('success'))
          <div class="alert alert-success">
          {{ Session::get('success') }}
          </div>
          @endif

          @if(Session::get('fail'))
              <div class="alert alert-danger">
              {{ Session::get('fail') }}
              </div>
          @endif

  
          <form action="{{route('confirmaddservices')}}" method="post">

            @csrf
            <span style="font-size: 10px" class="text-danger">@error('servicename'){{ $message }} @enderror</span>
          <div class="my-2">
              <input type="text" name="servicename" class="form-control" placeholder="Service Name">
          </div>

          <span style="font-size: 10px" class="text-danger">@error('servicedesc'){{ $message }} @enderror</span>
          <div class="my-2">
              <textarea style="resize:none"  rows="10" type="text" name="servicedesc" class="form-control" placeholder="Service Description" ></textarea>
          </div>

          <span style="font-size: 10px" class="text-danger">@error('tos'){{ $message }} @enderror</span>
          <div class="my-2">
            <span style="font-size: 10px">Type of Service:</span>
            <select class="form-select" name="tos" aria-label="Default select example">
              <option value="1">Bundle</option>
              <option value="0">Seperate</option>
            </select>
          </div>

          <div class="d-grid gap-2 mt-3">
            <button class="btn btn-primary btn-sm" type="submit">Add Service</button>
          </div>

          </form>

  
  
      </div>

      </div>

      {{-- <a href="logout" class="btn btn-primary">Logout</a> --}}

  </div>







    
</body>
</html>