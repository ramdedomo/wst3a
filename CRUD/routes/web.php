<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CRUDdb;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index'); 
});


Route::get('/register', function () {
    return view('register');
});

Route::post('/save',[CRUDdb::class, 'save'])->name('save');
Route::post('/check',[CRUDdb::class, 'check'])->name('check');
Route::post('/search',[CRUDdb::class, 'search'])->name('seach');
Route::get('/logout',[CRUDdb::class, 'logout'])->name('logout');
Route::post('/addappoint/{id}',[CRUDdb::class, 'addappoint'])->name('addappoint');
Route::get('/delete/{id}',[CRUDdb::class, 'delete'])->name('delete');
Route::get('/deleteService/{id}',[CRUDdb::class, 'deleteService'])->name('deleteService');
Route::get('/search',[CRUDdb::class, 'search'])->name('search');
Route::get('/occupied/{id}/',[CRUDdb::class, 'occupied'])->name('occupied');
Route::get('/approved/{id}/',[CRUDdb::class, 'approved'])->name('approved');
Route::get('/cancel/{id}/',[CRUDdb::class, 'cancel'])->name('cancel');
Route::post('/confirmaddservices',[CRUDdb::class, 'confirmaddservices'])->name('confirmaddservices');

Route::group(['middleware'=>['checkSession']], function(){
    Route::get('/',[CRUDdb::class, 'home']);
    Route::get('/login',[CRUDdb::class, 'login'])->name('login');
    Route::get('/index',[CRUDdb::class, 'index'])->name('index');
    Route::get('/yourappointments',[CRUDdb::class, 'yourappointments'])->name('yourappointments');
    Route::get('/register',[CRUDdb::class, 'register'])->name('register');
    Route::get('/profile/{id}',[CRUDdb::class, 'profile'])->name('profile');
    Route::get('/appoint/{id}',[CRUDdb::class, 'appoint'])->name('appoint');
    Route::get('/admin',[CRUDdb::class, 'admin'])->name('admin');
    Route::get('/adminappointments',[CRUDdb::class, 'adminappointments'])->name('adminappointments');
    Route::get('/adminprofile',[CRUDdb::class, 'adminprofile'])->name('adminprofile');
    Route::get('/addservices',[CRUDdb::class, 'addservices'])->name('addservices');
});