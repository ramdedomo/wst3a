<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Bio</title>

  <!-- EXTERNAL CSS LINKS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous" />
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;900&family=Roboto:wght@300;700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

  <!-- CUSTOM CSS -->
  <link href="css/style.css" rel="stylesheet" />

  <!-- BOOTSTRAP JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>

<body class="fonts">

  <section id="title">
    <div class="p-5">

      <div class="text-left mt-4 container">
        <h1 class="fonts2">ABOUT ME</h1>
        <h5>About Ram Dedomo</h5>
        <div class="btn_style mt-5">
          <a type="button" href="portfolio" class="btn btn-outline-light btn_styles">HOME</a>
          <a type="button" href="gallery" class="btn btn-outline-light btn_styles">GALLERY</a>
          <a type="button" href="contact" class="btn btn-outline-light btn_styles float-end">CONTACT</a>
        </div>
      </div>

    </div>
  </section>



  <section id="title2">

      <div class="second p-5">
      <div class="text-left container">
      <div class="second">
        <p class="artworks fonts2">
              My name is Ram Wendel S. Dedomo,
         </p>
        <p style="text-align: justify;" class="text-light">
          19 Years old from the Philippines.
          I am a 2nd Year BSIT ( Bachelor of Science in Information Technology) student at
          Pangasinan State University Urdaneta Campus. I am a 3d artist, my work includes modeling,
          texturing, and occasionally lighting and rendering. My hobbies is to play games whether
          on mobile or computer, MOBA (multiplayer online battle arena) and
          FPS (first person shooter) is my favorites genres of game.
        </p>
      </div>
      
    </div>

    <hr class="text-light container">

    <div class="second">
      <div class="container">
        <div class="row text-light">

          <div class="col-lg-6 col-sm-12 mt-3">
          <p class="m-0 fonts2">Work Experience</p>
          <br>
            <p class="m-0">&emsp;Freelance 3d Artist</p>
            <p class="m-0">&emsp;&emsp;Creating detailed 3d assets.</p>
            <p class="m-0">&emsp;&emsp;2019 - Present</p>
          </div>


          <div class="col-lg-6 col-sm-12 mt-3">
          <p class="m-0 fonts2">Trainings</p>
          <br>
            <p class="m-0">&emsp;Computer Systems Servicing NC II – TESDA</p>
            <p class="m-0">&emsp;&emsp;Mapandan National Senior High School</p>
            <p class="m-0">&emsp;&emsp;2019</p>
          </div>

        </div>

        <div class="row text-light mt-5">

          <div class="col-lg-6 col-sm-12 mt-3">
          <p class="m-0 fonts2">Hard Skills</p>
          <br>
            <p class="m-0">&emsp;Programming</p>
            <p class="m-0">&emsp;Data Analysis</p>
            <p class="m-0">&emsp;Web and App Development</p>
            <p class="m-0">&emsp;UI/UX Designer</p>
            <p class="m-0">&emsp;3D Modeling</p>
            <p class="m-0">&emsp;Video Editing</p>
          </div>


          <div class="col-lg-6 col-sm-12 mt-3">
          <p class="m-0 fonts2">Soft Skills</p>
          <br>
            <p class="m-0">&emsp;Communication</p>
            <p class="m-0">&emsp;Creativity</p>
            <p class="m-0">&emsp;Work Ethic</p>
            <p class="m-0">&emsp;Leadership</p>
          </div>

        </div>



    <hr class="text-light container mt-5">

        
      </div>
    </div>

    </div>
  </section>





  <footer class="text-center text-white" style="background-color: #181818;">
    <div class="p-3">
      <section>
        <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.facebook.com/RamDedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-facebook-f"></i></a>
        <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.instagram.com/ramdedomo" role="button" data-mdb-ripple-color="dark"><i class="fab fa-instagram"></i></a>
        <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.ramdedomo.artstation.com" role="button" data-mdb-ripple-color="dark"><i class="fab fa-artstation"></i></a>
        <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://github.com/ramdedomo" role="button" data-mdb-ripple-color="dark"><i class="fab fa-github"></i></a>
      </section>
    </div>
  </footer>




</body>

</html>