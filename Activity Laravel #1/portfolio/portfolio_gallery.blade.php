<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <title>Gallery</title>

    <!-- EXTERNAL CSS LINKS -->
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
      rel="stylesheet"
      integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
      crossorigin="anonymous"
    />
    <link rel="preconnect" href="https://fonts.gstatic.com" />
    <link
      href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;900&family=Roboto:wght@300;700&display=swap"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
      integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p"
      crossorigin="anonymous"
    />

    <!-- CUSTOM CSS -->
    <link href="css/style.css" rel="stylesheet"/>

    <!-- BOOTSTRAP JS -->
    <script
      src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js"
      integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf"
      crossorigin="anonymous"
    ></script>
  </head>

  <body class="fonts">


    <div class="p-5">
      <div class="text-left mt-4 container">
        <h1 class="fonts2 textcolor">GALLERY</h1>
        <h5 class="textcolor">Some of my Projects/Works</h5>
        <hr class="textcolor">
        <div class="btn_style mt-5">
          <a type="button" href="portfolio" class="btn btn-outline-dark">HOME</a>
        </div>
      </div>
    </div>

  <div class="p-5 pt-0">
    <div class="container mt-5">

      <div class="row">
        <div class="col-md-4 col-sm-12 mt-3">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/036/848/959/large/ram-dedomo-ice-post-1.jpg?1618805453" alt="pic1">
        </div>
        <div class="col-md-4 col-sm-12 mt-3">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/029/799/907/large/ram-dedomo-apple.jpg?1598711519" alt="pic2">
        </div>
        <div class="col-md-4 col-sm-12 mt-3">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/031/562/477/large/ram-dedomo-still.jpg?1615548244" alt="pic3">
        </div>
      </div>

      <div class="row mt-4">
        <div class="col-4">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdna.artstation.com/p/assets/images/images/029/800/810/large/ram-dedomo-front.jpg?1598711378" alt="pic1">
        </div>
        <div class="col-4">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdna.artstation.com/p/assets/images/images/029/800/808/large/ram-dedomo-side.jpg?1598676940" alt="pic2">
        </div>
        <div class="col-4">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdna.artstation.com/p/assets/images/images/029/800/806/large/ram-dedomo-top.jpg?1598676932" alt="pic3">
        </div>
      </div>

      <hr class="text-dark mt-5 mb-5">

      <div class="row">
        <div class="col-4">
          <img class="galleryPics" style='height: 100%; width: 100%;  border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/034/832/801/large/ram-dedomo-01-post.jpg?1615548290" alt="pic1">
        </div>
        <div class="col-4">
        <img class="galleryPics" style='height: 100%; width: 100%;  border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/033/896/429/large/ram-dedomo-untitled-01.jpg?1615548479" alt="pic1">
        </div>
        <div class="col-4">
          <img class="galleryPics" style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/039/432/763/large/ram-dedomo-up.jpg?1625878418" alt="pic2">
        </div>
      </div>


    </div>
        
  </div>

    <hr class="text-dark container">

    <p style="font-size: 10px;" class="container text-center fonts2">MORE ARTWORKS ON MY ARTSTATION PORTFOLIO!</p>
    
    
    <footer class="text-center text-white">
      <div class="p-5">
        <section>
          <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="https://www.facebook.com/RamDedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-facebook-f"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="https://www.instagram.com/ramdedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-instagram"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="https://www.ramdedomo.artstation.com/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-artstation"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-dark m-1" href="https://github.com/ramdedomo" role="button" data-mdb-ripple-color="dark"><i class="fab fa-github"></i></a>
        </section>
      </div>
    </footer>

  </body>
</html>
