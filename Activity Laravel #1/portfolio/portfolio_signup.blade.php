<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Contact</title>

  <!-- EXTERNAL CSS LINKS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous" />
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;900&family=Roboto:wght@300;700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

  <!-- CUSTOM CSS -->
  <link href="css/style.css" rel="stylesheet" />

  <!-- BOOTSTRAP JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>

<body class="fonts">
  <section id="title">
    <div class="p-5">
      <div class="text-left mt-4 container">
        <h1 class="fonts2">SIGNUP</h1>
        <hr class="text-light">
        <div class="btn_style mt-5">
          <a type="button" href="portfolio" class="btn btn-outline-light btn_styles">Home</a>
        </div>
      </div>
    </div>
  </section>


  <section id="title2">
    <div class="second p-5">
      <div class="container text-white">
        <div class="row">
          
          <div class="col-sm-12 col-lg-6">

          <form>
            <div class="row">
              <div class="mb-3 col-6">
                <label for="firstName" class="form-label">First Name</label>
                <input type="text" class="form-control " id="firstName" required>
              </div>

              <div class="mb-3 col-6">
                <label for="lastName" class="form-label">Last Name</label>
                <input type="text" class="form-control " id="lastName" required>
              </div>

              <div class="mb-3 col-3">
                <label for="age" class="form-label">Age</label>
                <input type="number" class="form-control " id="age" required>
              </div>

              <div class="mb-3 col-3">
                <label for="sex" class="form-label">Sex</label>
                <input type="text" class="form-control " id="sex" required>
              </div>

              <div class="mb-3 col-6">
                <label for="birthDate" class="form-label">Birthdate</label>
                <input type="date" class="form-control " id="birthDate" required>
              </div>

              <div class="mb-3 col-6">
                <label for="email" class="form-label">Email</label>
                <input type="email" class="form-control " id="email" required>
              </div>

              <div class="mb-3 col-6">
                <label for="password" class="form-label">Password</label>
                <input type="text" class="form-control " id="password" required>
              </div>

              <div class="mb-3">
                <button type="submit" class="btn btn-outline-light form-control-lg">SIGNUP</button>
              </div>  
              
          </div>
        </form> 

          </div>
        </div>
      </div>
    </div>
  </section>

  <footer class="text-center text-white" style="background-color: #181818;">
      <div class="p-3">
        <section>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.facebook.com/RamDedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-facebook-f"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.instagram.com/ramdedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-instagram"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.ramdedomo.artstation.com/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-artstation"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://github.com/ramdedomo" role="button" data-mdb-ripple-color="dark"><i class="fab fa-github"></i></a>
        </section>
      </div>
    </footer>





</body>

</html>