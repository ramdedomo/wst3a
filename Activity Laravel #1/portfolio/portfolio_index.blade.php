<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <title>Ram Dedomo</title>

  <!-- EXTERNAL CSS LINKS -->
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous" />
  <link rel="preconnect" href="https://fonts.gstatic.com" />
  <link href="https://fonts.googleapis.com/css2?family=Montserrat:wght@100;900&family=Roboto:wght@300;700&display=swap" rel="stylesheet" />
  <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

  <!-- CUSTOM CSS -->
  <link href="css/style.css" rel="stylesheet"/>

  <!-- BOOTSTRAP JS -->
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</head>
<style>
  .loginSignup{
    text-decoration: none;
    color: white;
    font-size: 15px;
  }

  .loginSignup:hover{
    color: lightblue;
  }


</style>

<body class="fonts">
  <section id="title">
    <div class="p-5">


      
        <div class="text-left mt-4 container">

          <h1 class="fonts2">HELLO! I'M RAM</h1>
          <h5>Student from Pangasinan State University.</h5>
          <a href="login" class="loginSignup">LOGIN</a> <span class="text-white">|</span>
          <a href="signup" class="loginSignup">SIGNUP</a>
          <hr class="text-light">
          <div class="btn_style mt-5">

            <a type="button" href="bio" class="btn btn-outline-light btn_styles">ABOUT</a>
            <a type="button" href="gallery" class="btn btn-outline-light btn_styles">GALLERY</a>
            <a type="button" href="contact" class="btn btn-outline-light btn_styles">CONTACT</a>

          </div>

          


          
        </div>

    </div>
  </section>


  <section id="title2">
    <div class="p-5 container">
      <div class="row">

        <div class="col-md-12 col-lg-6">
          <img class="imgpfp" src="images/pfp2.png">
        </div>

        <div class="col-md-12 col-lg-6">
          <div class="text-left bioIndex">
            <div class="second">
              <p class="artworks fonts2">
              My name is Ram Wendel S. Dedomo,
              </p>
              <p style="text-align: justify;" class="text">
              19 years old from Pangasinan, Philippines. I am 3rd year BSIT ( Bachelor of Science in Information Technology) student at Pangasinan State University Urdaneta Campus. My ...
              </p>
              <hr class="text-light">
              <div class="btn_style">
                <a type="button" href="bio" class="btn btn-outline-light btn_styles2">ABOUT</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>


      <section id="title3">
        <div class="p-5">
          <div class="container">

          <div class="row">

          
          <div class="col-lg-4 col-md-12">
              <div class="second">
                <h1 class="fonts2">MY ARTWORKS</h1>
                <h5>I am a 3D Artist, check out my projects through gallery!</h5>
                <hr class="text-light">
                  <div class="btn_style mt-5">
                    <a type="button" href="gallery" class="btn btn-outline-light btn_styles">GALLERY</a>
                  </div>
              </div>
            </div>

            <div class="col-lg-4 col-md-6 mt-3 art">
                <img style='height: 100%; width: 100%; object-fit: contain; border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/036/848/959/large/ram-dedomo-ice-post-1.jpg?1618805453"></img>
            </div>

            <div class="col-lg-4 col-md-6 mt-3 art">
                <img style='height: 100%; width: 100%; object-fit: contain;  border-radius: 5px;' src="https://cdnb.artstation.com/p/assets/images/images/029/799/907/large/ram-dedomo-apple.jpg?1598711519"></img>
            </div>



          </div>

          
        </div>
        </div>
      </section>



    <footer class="text-center text-white" style="background-color: #181818;">
      <div class="p-3">
        <section>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.facebook.com/RamDedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-facebook-f"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.instagram.com/ramdedomo/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-instagram"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://www.ramdedomo.artstation.com/" role="button" data-mdb-ripple-color="dark"><i class="fab fa-artstation"></i></a>
          <a class="btn btn-link btn-floating btn-lg text-light m-1" href="https://github.com/ramdedomo" role="button" data-mdb-ripple-color="dark"><i class="fab fa-github"></i></a>
        </section>
      </div>
    </footer>


</body>

</html>