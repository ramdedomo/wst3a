1. Put the portfolio folder in laravel/resources/view
2. Put the images folder and css folder in laravel/public
3. Put the web.php in laravel/routes OR add the following code to your existing web.php


Route::get('/portfolio', function () {
    return view('portfolio.portfolio_index');
});

Route::get('/bio', function () {
    return view('portfolio.portfolio_auto');
});

Route::get('/contact', function () {
    return view('portfolio.portfolio_contact');
});

Route::get('/gallery', function () {
    return view('portfolio.portfolio_gallery');
});

Route::get('/login', function () {
    return view('portfolio.portfolio_login');
});

Route::get('/signup', function () {
    return view('portfolio.portfolio_signup');
});



