<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/activity1', function () {
    return view('activity1');
});

Route::get('/portfolio', function () {
    return view('portfolio.portfolio_index');
});

Route::get('/bio', function () {
    return view('portfolio.portfolio_auto');
});

Route::get('/contact', function () {
    return view('portfolio.portfolio_contact');
});

Route::get('/gallery', function () {
    return view('portfolio.portfolio_gallery');
});

Route::get('/login', function () {
    return view('portfolio.portfolio_login');
});

Route::get('/signup', function () {
    return view('portfolio.portfolio_signup');
});