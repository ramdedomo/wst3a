<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
  body{
    zoom: 110%;
  }
</style>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>

      <body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">

                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/">Home</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/login">Login</a>
                  </li>

                </ul>
              </div>
            </div>
          </nav>



    </div>


    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="fs-6 badge text-bg-warning text-light">Welcome!</span>
            </div>
          
          </div>
    </div>




    <div class="container px-4 mt-4">

      <div class="rounded shadow p-3">

        <form action="/certvalidate" method="post" enctype="multipart/form-data">
          @csrf

          <span style="font-size: 10px" class="text-danger">@error('code_input'){{ $message }} @enderror</span>
          <span style="font-size: 12px; background-color: #002fa5;" class="mb-1 badge">INPUT CODE:</span>
          <input type="text" name="code_input" class="form-control" placeholder="$2y$10$bLAI ....">


    </div>
      <button type="submit" class="btn btn-primary mt-3" style="background-color: #002fa5;">Validate</button>
    </form>

    
    <div class="mt-3">
      @if(Session::get('success'))
      <div class="alert alert-success">
      {{ Session::get('success') }}   
      </div>

      @if(Session::get('ids'))
      <div class="card">
        <div class="card-body">
          <div class="row">
            <div class="col-12">
              <span class="badge text-light" style="background-color: #002fa5;">Paticipant Profile</span>
              <h5 class="m-0">{{ Session::get('ids')[0]->firstname . " " . Session::get('ids')[0]->lastname }}</h5>

              <span class="badge text-light mt-3" style="background-color: #002fa5;">Programme</span>
              <h5 class="m-0">{{ Session::get('ids')[0]->seminar_name }}</h5>
              <p>@php $created_at = new DateTime(Session::get('ids')[0]->date_generated); echo date_format($created_at, 'l jS F Y'); @endphp</p>
              <hr>
              
              <p class="card-text">You can redownload your certificate by clicking the following button</p>
              <a href="/download/{{ Session::get('ids')[0]->generated_id }}" style="background-color: #002fa5;" class="btn btn-sm text-light">Download</a>
            </div>
          </div>
        </div>
      </div>
      @endif

      @endif
  
      @if(Session::get('fail'))
          <div class="alert alert-secondary">
          {{ Session::get('fail') }}
          </div>
      @endif
    </div>
  

      </div>


    </div>


    



</body>
</html>

<style>

  .zoom{
    background-color: #002fa5;
  }

  .zoom:hover {
  background-color: #003cd4;
  transform: scale(1.01);
  transition: all 0.4s ease;/* (150% zoom - Note: if the zoom is too large, it will go outside of the viewport) */
}
</style>

