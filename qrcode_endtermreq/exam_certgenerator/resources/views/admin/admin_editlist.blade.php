<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

<style>
  body{
    zoom: 110%;
  }
</style>

      <!-- CSS only -->
      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
      <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.12.1/css/jquery.dataTables.css">
      <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.12.1/js/jquery.dataTables.js"></script>
      <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.3/font/bootstrap-icons.css">
      
      <script>
      $(document).ready(function () {
        $('#designs').DataTable();
      });
      </script>

<body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/admin">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">

                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="/admin">Home</a>
                      </li>
    
                      <li class="nav-item">
                        <a class="nav-link" href="/admin/editing">Edit Designs</a>
                      </li>
    
                      <li class="nav-item">
                        <a class="nav-link" href="/admin/seminars">Edit Seminars</a>
                      </li>

                      <li class="nav-item">
                        <a class="nav-link" href="/generatedcertificates">Generated Certificates</a>
                      </li>

                      <li class="nav-item">
                        <a href="/logout" type="button" class="p-0 m-0 btn btn-link btn-sm text-decoration-none mt-4">Logout</a>
                      </li>

                      


                </ul>
              </div>
            </div>
          </nav>
    </div>

    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="fs-6 badge text-light text-bg-warning">Logged as Admin</span>
            </div>
          
          </div>
    </div>



    <div class="container px-4 mt-3">
      <div class="rounded p-2">
        <h5>List of Design Certificates</h5>
      </div>
    </div>

        <div class="container px-4 mt-3">
          <div class="shadow p-3 rounded">
          <table id="designs" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Seminar</th>
                    <th>Description</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
            
                        @foreach ($designinfo as $design)
                        <tr>
                             <td>{{ $design->seminar_name }}</td>
                             <td>{{ Str::of( $design->seminar_desc )->limit(100); }}</td>
                             <td><a href={{ '/admin/editing/' . $design->design_id }} class="btn btn-primary"><i class="bi bi-pencil-fill"></i></a></td>
                        </tr>
                        @endforeach
                  
           
            </tbody>
        </table>
        
      </div>
        </div>



    
</body>
</html>

