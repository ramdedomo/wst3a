<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>

      <!-- CSS only -->
      <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
      <!-- JavaScript Bundle with Popper -->
      <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


<body>

    <div class="container">

        <nav class="navbar mt-5">
            <div class="container-fluid">
              <a class="navbar-brand" href="/editor">
                <img style="margin-top: 2px;" src="https://www.svgrepo.com/show/125020/qr-code.svg" alt="" width="30" height="24" class="d-inline-block align-text-top">

                <span>
                    Certificate Generator
                </span>
              </a>

              <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
              </button>
              <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav mt-4">

                  <li class="nav-item">
                    <a class="nav-link active" aria-current="page" href="/editor">Home</a>
                  </li>

                  <li class="nav-item">
                    <a class="nav-link" href="/editor/editing">Edit</a>
                  </li>

                </ul>
              </div>
            </div>
          </nav>
    </div>

  

    <div class="container px-4">
        <div class="row mt-3">
            <div class="col-6">
    
            </div>
            <div class="col-6 text-end">
                <span class="badge text-bg-success">Logged as Editor</span>
            </div>
          
          </div>
    </div>


    <div class="container px-4 mt-4">


      <div class="modal fade" id="newModules" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLabel">New Modules</h5>
              <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>

            <div class="modal-body">

              <form action="/insertModules" method="post" enctype="multipart/form-data">
                @csrf

                <span style="font-size: 10px" class="text-danger">
                  @error('type'){{ $message }}
                  
                  <script>
                    $(function() {
                        $('#newModules').modal('show');
                    });
                  </script>
                  
                  @enderror
                </span>
  
                <div class="mb-1" style="font-size: 15px;">Type:</div>
                <select name="type" id="type" class="form-control">
                  <option value="1">Logo</option>
                  <option value="2">Signature</option>
                </select>


                <div class="mt-3 mb-1" style="font-size: 15px;">Name:</div>
                <input name="name" type="text" class="form-control col-6">

                <div class="mt-3 mb-1" id="pos" style="font-size: 15px;">Position:</div>
                <input name="position" id="posinput" type="text" class="form-control col-6">

           
              <span style="font-size: 10px" class="text-danger">
                @error('file'){{ $message }}
                
                <script>
                  $(function() {
                      $('#newModules').modal('show');
                  });
                </script>
                
                @enderror
              </span>

              <div class="mt-3 mb-1" style="font-size: 15px;">Upload Photo:</div>
              <input name="file" type="file" class="form-control col-6">

            </div>

            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">Add</button>
            </div>

          </form>

          </div>
        </div>
      </div>




      <div class="row">
        <div class="card text-center col-6 p-2 mb-3">
          <div class="card-body">
        
            <h5 class="card-title">Design Modules</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a data-bs-toggle="modal" data-bs-target="#newModules" class="btn btn-primary">Add</a>
          </div>
        </div>
  
        <div class="card text-center col-6 p-2 mb-3">
          <div class="card-body">
            <h5 class="card-title">Edit Certificate</h5>
            <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
            <a href="/editor/editing" class="btn btn-primary">Edit</a>
          </div>
        </div>

      </div>


  </div>







    <footer class="fixed-bottom p-4  mb-5">
        <div class="container">
            <a href="/logout" type="button" class="btn btn-link btn-sm text-decoration-none float-end mt-2">Logout</a>
        </div>

    </footer>
    
</body>
</html>

<script>


  $( document ).ready(function() {

    if($( "#type option:selected" ).val() != 2){
          $("#pos").hide();
          $("#posinput").hide();
          
      }else{
        $("#pos").show();
          $("#posinput").show();
      }


      $( "#type" ).change(function() {
      console.log("asd");
      console.log($( "#type option:selected" ).text());
      console.log($( "#type option:selected" ).val());

      if($( "#type option:selected" ).val() != 2){
          $("#pos").hide();
          $("#posinput").hide();
          
      }else{
        $("#pos").show();
          $("#posinput").show();
      }

      });
  });
</script>